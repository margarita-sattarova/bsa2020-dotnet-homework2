﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;
using System.Threading;
using RabbitMQ.Wrapper.Settings;

namespace Pinger
{
    class Pinger
    {
        private readonly MessageConsumerScope _messageConsumerScope;
        private readonly MessageProducerScope _messageProducerScope;

        public Pinger(MessageConsumerScopeFactory messageConsumerScopeFactory, MessageProducerScopeFactory messageProducerScopeFactory)
        {
            _messageConsumerScope = messageConsumerScopeFactory.ListenQueue(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "ping_queue",
                RoutingKey = "*.queue.#"
            });

            _messageConsumerScope.MessageConsumer.Received += MessageReceived;

            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "pong_queue",
                RoutingKey = "pong_queue"
            });
        }

        public void Run()
        {
            Console.WriteLine("Pinger run");
        }

        private void MessageReceived(object sender, BasicDeliverEventArgs args)
        {
            var processed = false;

            try
            {
                var value = Encoding.UTF8.GetString(args.Body.ToArray());
                Console.WriteLine($"Got : {value}   at [{DateTime.Now}]");
                SendSuccessfulState();
                processed = true;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                processed = false;
            }
            finally
            {
                _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
            }
        }

        private void SendSuccessfulState()
        {
            Thread.Sleep(2500);

            Console.WriteLine($"Sent: Ping!   at [{DateTime.Now}]"); 

            _messageProducerScope.MessageProducer.SendMessageToQueue($"Ping!");
        }

        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(ConnectionSettings.Rabbit)
            };

            var messageConsumerScopeFactory = new MessageConsumerScopeFactory(factory);
            var messageProdScopeFactory = new MessageProducerScopeFactory(factory);

            var pinger = new Pinger(messageConsumerScopeFactory, messageProdScopeFactory);

            pinger.Run();
            pinger.SendSuccessfulState();

            Console.ReadKey();
        }
    }
}
