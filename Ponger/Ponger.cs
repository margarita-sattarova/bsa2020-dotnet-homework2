﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;
using System.Threading;
using RabbitMQ.Wrapper.Settings;

namespace Ponger
{
    class Ponger
    {
        private readonly MessageProducerScope _messageProducerScope;
        private readonly MessageConsumerScope _messageConsumerScope;

        public Ponger(MessageProducerScopeFactory messageProducerScopeFactory, MessageConsumerScopeFactory messageConsumerScopeFactory)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExchangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "ping_queue",
                RoutingKey = "topic.queue"
            });

            _messageConsumerScope = messageConsumerScopeFactory.ListenQueue(new MessageScopeSettings
            {
                ExchangeName = "ClientExchange",
                ExchangeType = ExchangeType.Direct,
                QueueName = "pong_queue",
                RoutingKey = "pong_queue"
            });

            _messageConsumerScope.MessageConsumer.Received += GetValue;
        }

        public bool PostValue(string value)
        {
            try
            {
                _messageProducerScope.MessageProducer.SendMessageToQueue(value);
                Console.WriteLine($"Sent:{value}   at [{DateTime.Now}]");
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        private void GetValue(object sender, BasicDeliverEventArgs args)
        {
            Thread.Sleep(2500);

            var value = Encoding.UTF8.GetString(args.Body.ToArray());

            Console.WriteLine($"Got: {value}   at [{DateTime.Now}]");

            _messageConsumerScope.MessageConsumer.SetAcknowledge(args.DeliveryTag, true);

            PostValue("Pong!");
        }

        public void Run()
        {
            Console.WriteLine("Ponger run");
        }

        static void Main(string[] args)
        {
            var factory = new ConnectionFactory()
            {
                Uri = new Uri(ConnectionSettings.Rabbit)
            };

            var messageProdScopeFactory = new MessageProducerScopeFactory(factory);
            var messageConsumerScopeFactory = new MessageConsumerScopeFactory(factory);

            var ponger = new Ponger(messageProdScopeFactory, messageConsumerScopeFactory);

            ponger.Run();

            Console.ReadKey();
        }
    }
}
