﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProducerScope
    {
        private readonly Lazy<MessageQueue> _messageQueueLazy;
        private readonly Lazy<MessageProducer> _messageProducerLazy;

        private readonly MessageScopeSettings _messageScopeSettings;
        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            _messageQueueLazy = new Lazy<MessageQueue>(CreateMessageQueue);
            _messageProducerLazy = new Lazy<MessageProducer>(CreateMessageProducer);
        }

        public MessageProducer MessageProducer => _messageProducerLazy.Value;

        private MessageQueue MessageQueue => _messageQueueLazy.Value;

        private MessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSettings);
        }

        private MessageProducer CreateMessageProducer()
        {
            return new MessageProducer(new MessageProducerSettings
            {
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(
                    _messageScopeSettings.ExchangeType,
                    _messageScopeSettings.ExchangeName,
                    _messageScopeSettings.RoutingKey)
            });
        }
    }
}
