﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumerScope
    {
        private readonly MessageScopeSettings _messageScopeSettings;
        private readonly Lazy<MessageQueue> _messageQueueLazy;
        private readonly Lazy<MessageConsumer> _messageConsumerLazy;

        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            _messageQueueLazy = new Lazy<MessageQueue>(CreateMessageQueue);
            _messageConsumerLazy = new Lazy<MessageConsumer>(CreateMessageConsumer);
        }

        public MessageConsumer MessageConsumer => _messageConsumerLazy.Value;

        private MessageQueue MessageQueue => _messageQueueLazy.Value;

        private MessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSettings);
        }

        private MessageConsumer CreateMessageConsumer()
        {
            return new MessageConsumer(new MessageConsumerSettings
            {
                Channel = MessageQueue.Channel,
                QueueName = _messageScopeSettings.QueueName
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
