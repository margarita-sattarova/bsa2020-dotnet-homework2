﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProducerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public MessageProducerScope Open(MessageScopeSettings messageScopeSettings)
        {
            return new MessageProducerScope(_connectionFactory, messageScopeSettings);
        }
    }
}
