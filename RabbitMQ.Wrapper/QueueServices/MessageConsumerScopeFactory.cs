﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumerScopeFactory
    {
        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScopeFactory(IConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public MessageConsumerScope Open(MessageScopeSettings messageScopeSettings)
        {
            return new MessageConsumerScope(_connectionFactory, messageScopeSettings);
        }

        public MessageConsumerScope ListenQueue(MessageScopeSettings messageScopeSettings)
        {
            var mqConsumerScope = Open(messageScopeSettings);
            mqConsumerScope.MessageConsumer.Connect();

            return mqConsumerScope;
        }
    }
}
